var express = require('express')
	app = express(),
	http = require('http'),
	config = require('./config/config'),
	path = require('path'),
	server = http.createServer(app),
	MessagesModel = require('./libs/mongoose').MessagesModel,
	Rooms = require('./libs/mongoose').RoomsModel,
	io = require('socket.io').listen(server);

server.listen(config.port, function(){
	console.log("Express server listening on port", config.port);
});

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
	res.sendFile('index.html');
});

app.get('/history', function(req, res){
	MessagesModel.find({}).sort({date: -1}).exec(function(error, docs) {
		if(error) {
			console.error(error);
		} else {
			res.send(docs)
		}
	});
});

io.sockets.on('connection', function (socket) {

	socket.on('adduser', function(user){
		socket.user = user;
		socket.room = 'general chat';
		var dateNow = new Date().toISOString();
		socket.broadcast.emit('updatechat', socket.user.username, socket.user.avatar, 'connected', dateNow);
	});
	
	socket.on('sendchat', function (data) {
		var dateNow = new Date().toISOString();
		io.sockets.emit('updatechat', socket.user.username, socket.user.avatar, data, dateNow);

		var message = new MessagesModel({
			username: socket.user.username,
    		avatar: socket.user.avatar,
    		text: data,
    		created: dateNow
    	});

    	message.save(function(error, data){
    		if(error) { 
                console.error(error);
            } else {
                console.log('message saved'); 
            }
    	});
    });
	socket.on('disconnect', function(){
		var dateNow = new Date().toISOString();
		socket.broadcast.emit('updatechat',  socket.user.username, socket.user.avatar, 'disconnected', dateNow);
	});
});
