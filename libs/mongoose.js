var mongoose    = require('mongoose'),
    config      = require('../config/config');

mongoose.connect(config.mongoose.uri);
var db = mongoose.connection;

db.on('error', function (err) {
    console.error('connection error:', err.message);
});
db.once('open', function callback () {
    console.info("Connected to DB!");
});

var Schema = mongoose.Schema;

var Messages = new Schema({
    username: {
        type: String,
        required: true
    },
    avatar: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    created: {
        type : Date, 
        default: Date.now 
    }
},{capped:{size: 70000000, max: 100} });

var Messages = mongoose.model('Messages', Messages);

module.exports.mongoose = mongoose;
module.exports.MessagesModel = Messages;
