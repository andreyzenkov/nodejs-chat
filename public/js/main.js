$(document).ready(function() {
	
	VK.init(function() {
	   console.log('VK init'); 
	}, function() { 
       throw new Error('Error vk init api');
    });

	var backend = 'http://95.85.37.101:8080/';
	var history = 'http://95.85.37.101:8080/history';
	var socket = io.connect(backend);

	$.get(history, function(data) {
		if(data.length) {
			var html = '';
			for(var i = 0, l = data.length; i < l; i++) {
				html += messageTemplate(data[i]);
			}
			$('.panel-body').append(html);
		}
	});

	function messageTemplate(m){
		return '<p><img src="' + m.avatar + '"> <span class="label label-default">' + m.username + '</span> <span class="label label-info">' + moment(m.created).toDate() + '</span> <span class="label label-success">' + m.text + '</span></p>';
	}
        function escape(s) {
            return ('' + s)
              .replace(/\\/g, '\\\\')
              .replace(/\t/g, '\\t')
              .replace(/\n/g, '\\n')
              .replace(/\u00A0/g, '\\u00A0')
              .replace(/&/g, '\\x26')
              .replace(/'/g, '\\x27')
              .replace(/"/g, '\\x22')
              .replace(/</g, '\\x3C')
              .replace(/>/g, '\\x3E');
        }
		
	socket.on('connect', function(){
		VK.api('users.get', {fields: 'id,photo_50,first_name'}, function(r) {
			socket.emit('adduser', {username: r.response[0].first_name, avatar: r.response[0].photo_50});
		});
	});

	socket.on('updatechat', function (username, avatar, data, dateNow) {
		var u = {
			username: username,
			avatar: avatar,
			created: dateNow,
			text: data
		};
		$('.panel-body').append(messageTemplate(u));
	});

	$('.send').click( function(event) {
		event.preventDefault();
		var message = $('.data').val();
		if(!message || message == ' ') return;

		$('.data').val('');
		socket.emit('sendchat', escape(message));
	});
});
